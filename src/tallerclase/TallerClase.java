/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerclase;

/**
 *
 * @author Estudiante
 */
import java.util.*;

public class TallerClase {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.util.Scanner input = new java.util.Scanner(System.in);
        ArrayList<Familia> familias = new ArrayList<>();
        boolean continuar = true;
        boolean primerIngreso = true;
        while (continuar) {
            System.out.println("Bienvenido al Sistema de registro familiar \n ¿Qué operación desea realizar?");
            System.out.println("\t 1. Ingresar una nueva familia. \n \t 2. Consultar familia. \n "
                    + "\t 3. Consultar miembro de familia. \n \t 4. Finalizar sesión.");
            int eleccion = input.nextInt();
            if (primerIngreso && eleccion != 1) {
                System.out.println("No ha ingresado familias al sistema, por favor ingrese una familia.");
                eleccion = 1;
            }
            switch (eleccion) {
                case 1:
                    ArrayList<Persona> hijos = new ArrayList<>();
                    System.out.println("Ingrese el apellido de la familia.");
                    String apellido = input.next();
                    System.out.println("Ingrese el nombre del padre.");
                    String nomb = input.next();
                    System.out.println("Ingrese la edad del padre.");
                    int edad = input.nextInt();
                    Persona padre = new Persona(nomb, apellido, edad, 'M');
                    System.out.println("Ingrese el nombre de la madre.");
                    nomb = input.next();
                    System.out.println("Ingrese la edad de la madre.");
                    edad = input.nextInt();
                    Persona madre = new Persona(nomb, apellido, edad, 'F');
                    System.out.println("Ingrese el numero de hijos.");
                    char sexo;
                    int numH = input.nextInt();
                    for (int i = 0; i < numH; i++) {
                        System.out.println("Ingrese el nombre del hijo " + (i + 1));
                        nomb = input.next();
                        System.out.println("Ingrese la edad del hijo " + (i + 1));
                        edad = input.nextInt();
                        System.out.println("Ingrese el sexo del hijo (M/F en mayúsculas, por favor)");
                        sexo = input.next().charAt(0);
                        Persona hijo = new Persona(nomb, apellido, edad, sexo);
                        hijos.add(hijo);
                    }
                    Familia f = new Familia(padre, madre);
                    f.setHijos(hijos);
                    familias.add(f);
                    primerIngreso = false;
                    break;
                case 2:
                    System.out.println("Ingrese el apellido de la familia que desea consultar. ");
                    apellido = input.next();
                    for (Familia i : familias) {
                        if (apellido == i.getPadre().getApellido()) {
                            System.out.println("Familia: " + apellido);
                            System.out.println(" \t Rol \t Nombre \t Apellido \t Edad \t Sexo");
                            System.out.print("\t Padre \t" + i.getPadre().getNombre() + "\t" + i.getPadre().getApellido() + "\t"
                                    + i.getPadre().getEdad() + "\t" + i.getPadre().getSexo());
                            System.out.print("\t Madre \t" + i.getMadre().getNombre() + "\t" + i.getMadre().getApellido() + "\t"
                                    + i.getMadre().getEdad() + "\t" + i.getMadre().getSexo());
                            ArrayList<Persona> hijosTMP = i.getHijos();
                            for (Persona h : hijosTMP) {
                                if (h.getSexo() == 'M' || h.getSexo() == 'm') {
                                    System.out.print("\t Hijo \t");
                                }
                                if (h.getSexo() == 'F' || h.getSexo() == 'f') {
                                    System.out.print("\t Hija \t");
                                }
                                System.out.println(h.getNombre() + "\t" + h.getApellido() + "\t" + h.getEdad() + "\t" + h.getSexo());
                            }
                        }
                    }
                    break;
                case 3:
                    System.out.println("Ingrese el apellido de la persona que desea consultar.");
                    apellido = input.next();
                    System.out.println("Ingrese el nombre de la persona que desea consultar.");
                    nomb = input.next();
                    for (Familia i : familias) {
                        if (apellido == i.getPadre().getApellido()) {
                            ArrayList<Persona> hijosTMP = i.getHijos();
                            for (Persona h : hijosTMP) {
                                if (h.getNombre() == nomb) {
                                    System.out.println("\t Nombre \t Apellido \t Edad \t Sexo \t Parentezco");
                                    System.out.println("\t" + h.getNombre() + "\t" + h.getApellido() + "\t" + h.getEdad() + "\t" + h.getSexo());
                                    System.out.print("\t" + i.getPadre().getNombre() + "\t" + i.getPadre().getApellido() + "\t"
                                            + i.getPadre().getEdad() + "\t" + i.getPadre().getSexo() + "\t Padre ");
                                    System.out.print("\t" + i.getMadre().getNombre() + "\t" + i.getMadre().getApellido() + "\t"
                                            + i.getMadre().getEdad() + "\t" + i.getMadre().getSexo() + "\t Madre ");
                                    for (Persona k : hijosTMP) {
                                        if (k.getNombre() != h.getNombre()) {
                                            System.out.println("\t" + h.getNombre() + "\t" + h.getApellido() + "\t"
                                                    + h.getEdad() + "\t" + h.getSexo());
                                            if (h.getSexo() == 'M' || h.getSexo() == 'm') {
                                                System.out.print("\t Hermano \t");
                                            }
                                            if (h.getSexo() == 'F' || h.getSexo() == 'f') {
                                                System.out.print("\t Hermana \t");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 4:

                    continuar = false;
                    break;
                default:
                    System.out.println("Coja oficio, ingrese un valor valido.");
            }
        }

    }

}
