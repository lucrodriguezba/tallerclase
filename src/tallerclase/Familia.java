/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerclase;

/**
 *
 * @author Estudiante
 */
import java.util.*;
public class Familia {
	private Persona padre;
	private Persona madre;
	private ArrayList<Persona> hijos = new ArrayList<>();

	public Familia(Persona padre, Persona madre) {
    	this.padre = padre;
    	this.madre = madre;
	}

	public void setHijos(ArrayList<Persona> hijos) {
    	this.hijos = hijos;
	}

	public Persona getPadre() {
    	return padre;
	}

	public Persona getMadre() {
    	return madre;
	}

	public ArrayList<Persona> getHijos() {
    	return hijos;
	}
    
}
